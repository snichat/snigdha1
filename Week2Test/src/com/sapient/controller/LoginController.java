package com.sapient.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.exception.InvalidId;
import com.sapient.exception.PasswordInvalid;
import com.sapient.model.CustomerBean;
import com.sapient.model.CustomerDAO;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }
   
   ServletContext cn;
   @Override
   public void init(ServletConfig config) throws ServletException
   {
	   cn=config.getServletContext();
	   CustomerDAO c=new CustomerDAO();
	   cn.setAttribute("list", c.load());
	   super.init(config);
	}
    
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		try 
		{
			String id=request.getParameter("id");
			if(id==null)
			{
				throw new InvalidId(ResourceBundle.getBundle("Exception").getString("error2"));
			}
			CustomerBean cb=(CustomerBean)((Map<String,CustomerBean>) cn.getAttribute("list")).get(id);
			
			RequestDispatcher rd=request.getRequestDispatcher("area.jsp");
			request.setAttribute("msg", cb.getName());
			rd.forward(request, response);
			
			
		}
		catch (InvalidId i) 
		{
			RequestDispatcher rd=request.getRequestDispatcher("main.jsp");
			request.setAttribute("msg",i.getMessage() );
			rd.forward(request, response);
			
		}

		
	}

}
