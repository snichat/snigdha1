package com.sapient.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.rest.client.APICall;

/**
 * Servlet implementation class AreaController
 */
public class AreaController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AreaController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String city=request.getParameter("city");
		String msg=request.getParameter("msg");
		System.out.println(msg);
		
		APICall call=new APICall();
		String list=APICall.caller(city);
		
		String[] l=list.split("establishment");
		
		RequestDispatcher rd=request.getRequestDispatcher("area.jsp");
		request.setAttribute("list", l);
		request.setAttribute("msg", msg);
		rd.forward(request, response);
		System.out.println(msg);
		
		
	}

}
