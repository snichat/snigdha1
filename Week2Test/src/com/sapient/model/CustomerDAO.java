package com.sapient.model;

import java.util.HashMap;
import java.util.Map;

public class CustomerDAO 
{
	public Map<String,CustomerBean> load()
	{
	
	CustomerBean c1=new CustomerBean("1","Rahul","hi1");
	CustomerBean c2=new CustomerBean("2","Riya","hi2");
	CustomerBean c3=new CustomerBean("3","Snigdha","hi3");
	CustomerBean c4=new CustomerBean("4","Simran","hi4");
	CustomerBean c5=new CustomerBean("5","Vaidika","hi5");
	
	Map<String,CustomerBean> map= new HashMap<String,CustomerBean>();
	map.put("1", c1);
	map.put("2", c2);
	map.put("3", c3);
	map.put("4", c4);
	map.put("5", c5);
	
	return map;
	
		
	}

}
