package com.sapient.model;

public class CustomerBean
{
	private String customer_id;
	private String name;
	private String password;
	public CustomerBean(String customer_id, String name, String password) {
		super();
		this.customer_id = customer_id;
		this.name = name;
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public CustomerBean() {
		super();
	}
	

}
