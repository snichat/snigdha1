package com.sapient.week1;

import java.util.*;

public class Workspace {
	private int[] l;
	public Workspace()
	{
		this.l=new int[10];
	}
	public Workspace(int num[])
	{
		this.l=new int[num];
	}
	public void display()
	{
		Arrays.stream(l).forEach(System.out::print);
		System.out.println(" ");
	}
	public void sort()
	{
		Arrays.sort(l);
	}
	public void search(int a)
	{
		int i;
		for (i=0;i<l.length;i++)
		{
			if(l[i]==a)
			{
				System.out.println("Found");
				break;
			}
		}
	}
	public void avg()
	{
		int i,s=0;
		for (i=0;i<l.length;i++)
		{
			s+=l[i];
		}
		System.out.println(s/l.length);
	}	
}
