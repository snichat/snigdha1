
public class PlayerBean {
	private int id;
	private String fname;
	private String lname;
	
	public PlayerBean(String fname, String lname, int id) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
	}
	public PlayerBean() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	
}

