
public class generics {

	public <E> void display(E s[]) 
	{
		int i;
		for(i=0;i<s.length;i++)
		{
			System.out.print(s[i]);
			System.out.print(" ");
		}
		System.out.println();
	}

	public static void main(String[] args) 
	{
		generics e=new generics();
		Integer num[]= {1,2,3,4};
		String name[]= {"sum","sni","son"};
		e.display(num);
		e.display(name);
		
	}

}
