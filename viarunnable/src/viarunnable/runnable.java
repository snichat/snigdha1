package viarunnable;

public class runnable 
{	
	public static char flag='F';
	public static int count=0;
	static class A implements Runnable
	{
		public void run ()
		{
			while(count<9)
			{
				if(flag=='F')
				{
					System.out.print("TICK--");
					flag='T';
					count++;
				}
			}
			
		}
	}

	static class B implements Runnable
	{
		public void run ()
		{
			while(count<10)
			{
				if(flag=='T')
				{
					System.out.println("TOCK");
					flag='F';
				}
				
			}
			
		}
		
	}
public static void main(String[] args) 
	{
		Thread t1=new Thread(new A());
		Thread t2=new Thread(new B());
		t1.start();
		t2.start();
	}


}
