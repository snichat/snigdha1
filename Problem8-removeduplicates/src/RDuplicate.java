import java.util.Arrays;

public class RDuplicate {

	public static void main(String[] args) {
		int i[]= {1,2,3,1,1,3,2,4,5};
		int x[]=Arrays.stream(i).distinct().toArray();
		Arrays.stream(x).forEach(System.out::println);
		int s=Arrays.stream(i).sum();
		System.out.println(s);
	}
}
