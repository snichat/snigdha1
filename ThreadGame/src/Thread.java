public class Thread {

		public static char flag='F';
		public static int count=0;
		static class A implements Runnable
		{
			public void run ()
			{
				while(count<9)
				{
					if(flag=='A')
					{
						System.out.print("TICK--");
						flag='B';
						count++;
					}
				}
				
			}
		}

		static class B implements Runnable
		{
			public void run ()
			{
				while(count<10)
				{
					if(flag=='B')
					{
						System.out.println("TOCK");
						flag='C';
					}
					
				}
				
			}
			
		}

		static class C implements Runnable
		{
			public void run ()
			{
				while(count<10)
				{
					if(flag=='C')
					{
						System.out.println("TOCK");
						flag='A';
					}
					
				}
				
			}
			
		}
	public static void main(String[] args) 
		{
			Thread t1=new Thread(new A());
			Thread t2=new Thread(new B());
			Thread t3=new Thread(new C());
			t1.start();
			t2.start();
			t3.start();
		}


	}

}
