import java.util.Scanner;

public class Controller 
{
	public static void main(String args[])
	{
		Model model=new Model();
		View view;
		BusinessModelDOA doa;
		
		view=new View(model);
		doa=new BusinessModelDOA(model);
		view.display(model);
	}

}
