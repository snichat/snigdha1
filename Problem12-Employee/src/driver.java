import java.util.Scanner;

public class driver {

	public static void main(String[] args) 
	{
		EmployeePOJO emp1=new EmployeePOJO("snigdha",21,"bhopal",50000);
		EmployeePOJO emp2=new EmployeePOJO("sidharth",20,"bhopal",500000);
		EmployeePOJO emp3=new EmployeePOJO("geeta",19,"bhopal",5000000);
		
		EmployeePOJO l[]= {emp1,emp2,emp3};
		System.out.println("Give SQL Query : ");
		Scanner scan=new Scanner(System.in);
		String s=scan.nextLine();
		SQLParser sql=new SQLParser();
		boolean x=sql.solver(s);
		if(x)
		{
			CSV csv=new CSV();
			csv.display(l,SQLParser.disp);
		}
		scan.close();
		
	}

}
